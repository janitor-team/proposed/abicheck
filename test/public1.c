/*
 * Copyright (c) 2001 by Sun Microsystems, Inc.
 * All rights reserved.
 */

/* This program should always pass. It only uses public syms, see run_tests */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <stdio.h>

main() {
	char str[] = "a_msg";

	int fd = open("/dev/null", O_WRONLY);

	write(fd, str, 5);
	fprintf(stderr, "fd: %d str:%s\n", fd, str);
	
	return 0;
}
