/*
 * Copyright (c) 2001 by Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/* This is used to catch libc.a linking. See Makefile & run_tests */

#include <stdlib.h>
#include <math.h>

main() {
	/* libm interface is called just to get some dynamic binding */
	printf("%f\n", cos(0.0));	
	exit(3);
}
