/*
 * Copyright (c) 2001 by Sun Microsystems, Inc.
 * All rights reserved.
 */

/* This is for simple catching of some private symbol usage. See run_tests */

#include <stdio.h>

#if	defined(SunOS)
#define OS "Solaris"
#include <time.h>
	extern int __nanosleep(const struct timespec *r, struct timespec *m);

#elif 	defined(Linux)
#define OS "Linux"
#define O_WRONLY 1
	typedef int mode_t;
	extern int __open(__const char *file, int oflag, mode_t mode);
	extern int __open_catalog(void);  /* FIXME get real signature */
#else
#error "unsupported OS"

#endif
	
main() {
	char str[] = "abc";
	fprintf(stderr, "running on: %s\n", OS);
#if	defined(SunOS)
	__nanosleep(0, 0);
#elif 	defined(Linux)
	__open("/dev/null", O_WRONLY, 0644);
	if ( strlen(str) == 2 ) {
		__open_catalog();	/* just to get a binding ... */
	}
#endif
	fprintf(stderr, "Done.\n");

	return(0);
}
