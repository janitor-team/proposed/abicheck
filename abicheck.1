.nr X
.TH abicheck 1 "26 August 2003"
.SH NAME
abicheck \- check application binaries for calls to private or evolving symbols
in libraries and for static linking of some system libraries.
.\"
.SH SYNOPSIS
.B abicheck
[-h]
[-k]
[-a]
[-I]
[-v]
[-f
.I listfile\]
[-o
.I outfile\]
[-p
.I pattern\]
[-e
.I pattern\]
[-j
.I njobs\]
[-l
.I library\]
[-L
.I ldpath\]
[(-s|-S)
.I dbfile\]
[(-d|-D)
.I dbfile\]
[-O
.I dbfile\]
[-A
.I listfile\]
.I files
.SH DESCRIPTION
.LP
.B abicheck
is run on application binaries and issues warnings whenever
any of the following three conditions are detected:
.LP
\(bu
.I Private symbol usage.
Private symbols are functions or data variables in a library package
that are internal to that package. They are used by the libraries
in the package for internal communication and are not part of the
API/ABI that application developers should use.
.LP
\(bu
.I Evolving symbol usage.
Evolving symbols are functions or data variables in a library
package that are intended for developer consumption, but have been
marked as "evolving" or "unstable" in the sense that they may become
incompatible or disappear on a later release of the library package.
.LP
\(bu
.I Static linking.
Static linking of system libraries (for example, libc.a) into an application
is generally not a good idea because the system library code it
"locks" into the application binary may become incompatible with
later releases of the system. abicheck attempts to detect static
linking of a few system libraries.
.LP
The default behavior is, for each binary object checked,
to examine direct calls from that binary object only. The
.B \-l
option allows the libraries the binary object brings in to
have their calls checked as well.
.SH OPTIONS
The following options are supported:
.TP
.B \-k
Keep on checking binaries even if there are
serious errors (dynamic linker reports unresolved
symbols,
.BR ldd (1)
failures, no symbols detected).
.TP
.B \-h
Print out long form of help.
.TP
.B \-v
Verbose.  Print out additional information.
.TP
.BI \-f \ listfile
The
.I listfile
is a file containing a list of binary objects
to check, one per line. This list is appended
to any files provided as arguments on the command line.
If 
.I listfile
is "\-", then stdin is used.
.TP
.BI \-o \ outfile
Write output to 
.I outfile
instead of stdout.
.TP
.BI \-p \ pattern
Modify the version name pattern match labelling
private version sets. Default is
.B /private/
using a case-insensitive match.
.sp
If a component of the regex
.I pattern
contains two colons in a row:
.BR patt1::patt2 ,
then symbol-level matching will be activated by
checking whether
.I version::symbol
or
.I library::symbol
matches
.I pattern
(where the symbol name, version (if any), and
library basename are substituted for
.IR symbol ,
.IR version ,
and
.IR library ).
For example,
.sp
.RS
    -p 'FOO_VERS.*::_foopriv'
.br
or
.br
    -p 'libfoo\.so.*::_foopriv'
.RE
.TP
.BI \-e \ pattern
Same as 
.B \-p
but  for "evolving" interfaces.
.TP
.BI \-L \ ldpath
Set the
.B LD_LIBRARY_PATH
environment variable to
.I ldpath
before invoking dynamic linker.
Use 
\fB\-L \fR""
to unset 
.B LD_LIBRARY_PATH.

If one of the components of 
.I ldpath
is the
string "find", then all shared libraries in
.I files
are found and their paths inserted into
the "find" location.  Note that the order will
random.
.TP
.BI \-l \ library
Add the basename or full pathname of the shared library
.I library
to the list of objects to be checked for making private
calls. This option may occur more than once
on the command line and is additive.
By default, only direct
calls from a binary to the system libraries are
checked.  The 
.B \-l
switch allows checking of indirect
calls e.g.: app -> supportlib -> systemlib.
.TP
.B \-a
Loop through all of the binaries before checking
and collect the list of all shared objects.
Take the basename of each shared object found
and act as though it was specified with the 
.B \-l
option option and then run the abicheck checks.
This way, calls from all "application internal"
objects are checked rather than just the direct
calls.  (Useful when shared objects do not have
their dependencies recorded.)
.TP
.BI \-I
Ignore shared libraries in checking, only
check executables.  Compatible with 
.B \-a,
libraries
will be searched for first but then not checked.
.TP
.BI \-d \ dbfile, \ \-D \ dbfile
Specify fallback flat-file symbol database for
the dynamic (public vs. private) test.  These
classifications will be used if the library is not
versioned (i.e. classification does not exist
in the library itself).  Use 
.B \-D
to indicate that
only information from 
.I dbfile
should be used.
Lines in 
.I dbfile
can be of one of these forms:
.sp
	library|symbol
.br
	library|class|symbol
.br
	library|FILE=path
.sp
.I library
must be the full path to the library
to be specified (it cannot be a basename).
.sp
The first form marks 
.I symbol
as private.
.sp
The second form marks 
.I symbol
with 
.I class
where 
.I class
may be public, private, or evolving.
.sp
The third form indicates the file 
.I path
should
be opened on demand when 
.I library
is first
encountered.  File 
.I path
contains lines of the
first two forms except for the 
.I library
field.
The third form is a speedup to avoid
processing many classification lines for
libraries never encountered in the run.
.TP
.BI \-O \ dbfile
Specify an override file to modify the symbol
classification for the dynamic (public vs. private)
test.  The format for the override file is like:
.sp
	library|symbol|class
.sp
The library can be the full path or basename.
If library is "__SKIP__" the symbol will be ignored
for any library it is found in.  The class can be
"public", "private", "evolving", or "deleted".
The "deleted" class is special-cased, means the
symbol was deleted from the library on some release.
The symbol "__ALL__" for the "deleted" class means
the entire library was deleted or is otherwise
unstable to use.
.sp
Examples:
.sp
	libfoo.so.1|__bar|private
.br
	/lib/libxyz.so.1|baz|public
.br
	__SKIP__|__fputwc_xpg5
.sp
These settings override any classification
inside the library (from library versioning,
obtainable from 
.BR pvs (1),
etc).
.TP
.BI \-A \ listfile
Set the ABI libraries of interest to the libraries
listed in 
.I listfile
(full pathnames, one per line).
Only calls into these libraries will be checked;
all other library calls will be ignored.
.TP
.BI \-s \ dbfile, \ \-S \ dbfile
Specify more extensive symbol databases for the
static linking test.  
.I dbfile
may be a comma
separated list of files.  If a file is a static
archive (lib*.a) it is processed to extract the
symbols.  Otherwise it is a database file consisting
of lines of the form symbol|library:module
for example:
.sp
        shmat|/usr/lib/libc.a:shmsys.o
.br
        shmctl|/usr/lib/libc.a:shmsys.o
.br
        shmdt|/usr/lib/libc.a:shmsys.o
.br
        shmget|/usr/lib/libc.a:shmsys.o
.br
        ...
.sp
When all symbols in a module.o are defined in
the application, static linking of that module
(and corresponding library archive) is
assumed.  Use 
.B \-S
to indicate that only the
static link test should be performed.
.sp
Use 
.B \-S  \ int
to do only the static link check
and using the internal database.
.sp
Use 
.B \-s \ none
or
.B \-S \ none
to skip the static
linking check entirely.
.TP
.BI \-j \ njobs
Run 
.I njobs
in parallel as separate processes.
Implies 
.BR \-k \.
Primarily intended for multiple CPU machines where 
.I njobs
should be close to
the number of processors.  Output is collected
in tmp files and printed all at once near the
end of the run as each job finishes.
.sp
If 
.I njobs
is "-", "detect", or "n", then 
.I njobs
will be set to a number depending on the number
of processors on the current machine (if that
can be determined).
.SH OPERANDS
.LP
The following operands are supported:
.TP
.I files
A list of application binary objects to check.
.SH OUTPUT
.LP
There is one line per problem (there may be multiple problems
per binary checked) which look like the following: 
.sp
.RS
If no problems were found:
.br
    \fIfilename\fR: OK
.sp
If private symbol usage:
.br
    \fIfilename:\fR PRIVATE
(\fIlibrary\fR:\fIprivate_version\fR) \fIprivate_sym\fR
.sp
If evolving symbol usage:
.br
    \fIfilename\fR: EVOLVING (\fIlibrary\fR:\fIevolving_vers\fR) \fIevolving_sym\fR
.sp
If file statically linked in a system archive library:
.br
    \fIfilename\fR: STATIC_LINK (\fIarchive\fR)
.sp
If checking of the file was skipped:
.br
    \fIfilename\fR: SKIP (\fIreason\fR)
.RE
.sp
.LP
Under use of the deleted class in the 
.B -O
override file option, these
problems may be found:
.sp
.RS
If a symbol has been deleted from the library on some release:
.br
    \fIfilename\fR: DELETED_SYM: \fIsymbol\fR/\fIlibrary\fR
.sp
(\fIlibrary\fR will be "unbound" if the symbol was unbound)
.sp
If an entire library has been deleted on some release or is
otherwise unstable to use:
.br
   \fIfilename\fR: UNSTABLE_LIB: \fIlibrary-soname\fR =\fR \fIlibrary-path\fR
.sp
(\fIlibrary-path\fR may be "file not found" if the library could not
be found)
.RE
.sp
.LP
The following problems will cause a fatal error unless the
.B \-k
option is used:
.sp
.RS
If the dynamic linker could not resolve
.I N
symbols when
.B ldd \-r
was run:
.br
    \fIfilename\fR: UNBOUND_SYMBOLS: \fIN\fR
.sp
If the dynamic linker found no dynamic bindings:
.br
    \fIfilename\fR: NO_BINDINGS
.sp
If
.B ldd \-r
with
.B LD_DEBUG=files,bindings
failed:
.br
    \fIfilename\fR: LDD_ERROR
.RE
.LP
In these latter three cases run
.B ldd \-r
on the binary file for
more information on what went wrong (note that
.B abicheck
runs
.B ldd \-r
with
.B LD_DEBUG=files,bindings
set). On some systems the dynamic linker
will not process SUID programs with
.B LD_DEBUG
set (this usually results in
.B NO_BINDINGS
in the
.B abicheck
output).
.LP
Note that if you are running abicheck on a shared library
(for example, libfoo.so) that has
.I not
been built with
.B \-l
.I lib
flags to record its library dependencies, then the "unbound symbols"
problem is very likely. There is not much that can be done besides
rebuilding the library or checking an application binary that
uses the library and using the
.B \-l
option of
.B abicheck.
.SH EXIT STATUS
.LP
The following exit values are returned:
.TP
.B 0
No errors and no problems found.
.TP
.B 1
A fatal error occurred.
.TP
.B 2
No fatal errors occurred, but some binaries had problems detected.
.SH NOTES
.LP
Only ELF objects are checked.
.LP
In the 
.BR \-s \,
.BR \-S \,
.BR \-d \,
and
.B \-O
dbfiles the '#' character starts
a comment line in the usual way.
.LP
Unless one is using the "::" custom matches supplied via the 
.B \-p
or
.B \-e
flags, abicheck can only check against system libraries that 
have had symbol versioning applied to them (i.e. the private and/or
evolving information recorded for each symbol in the library
itself). For more info about symbol versioning, see the 
"Solaris Linker and Libraries Guide" answerbook at the URL
http://docs.sun.com/ab2/coll.45.13 and the Commands/Version-Script
section of the GNU linker "ld" info page.
.LP
The default symbol version name matching patterns are case
insensitive matches to the strings "private" and "evolving" for
the private and evolving cases, respectively.
.LP
Odd filenames containing the single-quote character or newline
will be skipped; such characters interfere with calling commands via the shell.
.LP
To recurse directories use
.BR find (1)
and either collect the
output to a file for use with the 
.B \-f
option, or in a pipe in via:
.sp
        find ... | abicheck -f - ...
.SH BUGS
The program is dependent on the form of the runtime linker's 
debug output.  Since this output is intended to be human readable
rather than machine readable, 
.B abicheck
will break whenever the output format changes.  On Solaris it
is possible that the Link Auditing C interface could be used
to avoid this problem.
.LP
On Linux when 
.BR ldd (1)
is run on a SUID binary, it (ldd and the
dynamic-linker) will sometimes actually run the binary. On Linux
SUID/SGID binaries are currently skipped even if the user is root;
test unprivileged copies instead.
.SH SEE ALSO
.BR ld (1),
.BR ldd (1),
